<?php

namespace App;

/**
 * A class that handles tags
 */
class Tag extends Model
{
    /**
     * Declare a one to many relationship between a tag and a contact
     */
    public function contacts()
    {
        return $this->belongsToMany(Contact::class);
    }

    /**
     * Allow user to type tag name in URL instead of
     */
    public function getRouteKeyName()
    {
        return 'name';
    }
}
