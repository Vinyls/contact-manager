<?php

namespace App\Http\Controllers;

use League\Csv\Reader;
use App\Contact;

/**
 * Handle uploaded CSVs
 */
class CsvController extends Controller
{
    /**
     * Create contacts for each row of data in the uploaded CSV
     */
    public function store()
    {
        $uploaded_file = request()->file('csv'); // Get the uploaded CSV
        $uploaded_file->storeAs('csv', 'uploaded_csv.csv'); // Store it in the filesystem

        $csv = Reader::createFromPath( storage_path() . '/app/csv/uploaded_csv.csv' ); // Read the CSV file

        // Create a contact for each row of data in the CSV
        foreach ($csv as $index => $row) {
            Contact::create([
                'first_name' => $row[0],
                'surname' => $row[1],
                'home_address' => $row[2],
                'email_address' => $row[3],
                'phone_number' => $row[4],
                'website' => $row[5]
            ]);
        }

        unlink( storage_path() . '/app/csv/uploaded_csv.csv' ); // Remove the file once done

        return back()->with('success', "Contact(s) Successfully Imported"); // Redirect to the homepage with success message
    }
}
