<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Tag;

class ContactsController extends Controller
{
    /**
     * Load home view with contacts in DB listed alphabetically
     */
    public function index()
    {
        $contacts = Contact::firstNameAsc()->get();

        $tags = Tag::all();

        return view('home', compact('contacts', 'tags'));
    }

    /**
     * Add a new contact from form
     */
    public function store()
    {
        $this->validate(request(), [
            'first-name' => 'required',
            'surname' => 'required',
        ]);

        // Take form fields and store them in the DB
        Contact::create([
            'first_name' => request('first-name'),
            'surname' => request('surname'),
            'home_address' => request('home-address'),
            'email_address' => request('email_address'),
            'phone_number' => request('phone_number'),
            'website' => request('website')
        ]);

        return redirect('/')->with('success', 'Contact Successfully Added'); // Redirect to the homepage with success message
    }

    /**
     *  Load single contact view with the current contact
     */
    public function show(Contact $contact)
    {
        $tags = Tag::all();

        return view('contact.show', compact('contact', 'tags'));
    }
}
