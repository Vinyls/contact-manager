<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Contact;

class TagsController extends Controller
{
    public function index(Tag $tag)
    {
        $contacts = $tag->contacts;
        $tags = Tag::all();

        return view('home', compact('contacts', 'tags'));
    }

    /**
     * Create a tag
     */
    public function store()
    {
        $this->validate(request(), [
            'tag-name' => 'required',
        ]);

        Tag::create([
            'name' => request('tag-name'),
        ]);

        return back();
    }

    /**
     * Assign a tag to a contact
     */
    public function assign()
    {
        $contact_id = request('contact-id');

        $contact = Contact::find($contact_id);

        $contact->tags()->attach(request('tag-id'));

        return back();
    }
}
