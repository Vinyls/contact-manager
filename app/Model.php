<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Parent Class - All other classes extend this
 */
class Model extends Eloquent
{
    protected $guarded = []; // No form fields are blocked form being submitted
}
