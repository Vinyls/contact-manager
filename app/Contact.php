<?php

namespace App;

/**
 * A class that handles contacts
 */
class Contact extends Model
{
    /**
     * Retrieves contact from DB in ascending alphabetical order by the contacts first name
     */
    public function scopeFirstNameAsc($query)
    {
        return $query->orderBy('first_name', 'asc');
    }

    /**
     * Declare a one to many relationship between a contact and a tag
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}
