jQuery(document).ready(function($) {
  // Initialise datatable on the contact table
  function initiateDatatable() {
    $('table.contacts').DataTable({
      "language": {
        "emptyTable": "You don't have any contacts yet. Add one by clicking the 'Add Contact' button."
      }
    });
  }

  initiateDatatable();
});