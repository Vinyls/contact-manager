@extends('layout')

@section('content')
    <div class="container-fluid site-wrap">
        <header>
            <h1>Contact Manager</h1>
        </header>

        <main class="row container">
            <a href="/">
                <button class="btn btn-primary">Home</button>
            </a>

            <h2>{{ $contact->first_name }} {{ $contact->surname  }}</h2>
            <p><strong>Home Address:</strong><br>{{ $contact->home_address }}</p>
            <p><strong>Added:</strong> {{ $contact->created_at->toFormattedDateString() }}</p>

            @include('partials.errors')

            <hr>

            <h3>Add tag to contact</h3>

            @include('forms/assign-tag')
        </main> <!-- .row -->
    </div> <!-- .container-fluid-->
@endsection