@if (!$tags->isEmpty())
    <div class="tags">
        <p class="tags-title">Filter contacts by a tag:</p>
        <ul>

            @if (Request::is('contacts/tags/*'))
                <a class="btn btn-xs remove-filter" href="/">
                    <li>Remove Filter</li>
                </a>
            @endif

            @foreach ($tags as $tag)
                <a class="btn btn-info btn-xs" href="/contacts/tags/{{ $tag->name }}">
                    <li>{{ $tag->name }}</li>
                </a>
            @endforeach

        </ul>
    </div>
@endif