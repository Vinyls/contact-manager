<form class="import-contact" name="csv-importp" method="POST" action="/csv" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="first-name">Attach a CSV:</label>
        <input type="file" id="csv" name="csv" required>
        <a class="btn btn-info btn-xs sample-csv" href="contacts.csv">Download sample CSV file</a>
        <hr>

        <span id="helpBlock" class="help-block">
            <strong>Note:</strong> Please upload a CSV with the following data in each cell of a row
            <ul>
                <li>First Name</li>
                <li>Surname</li>
                <li>Home Address</li>
                <li>Email Address</li>
                <li>Phone Number</li>
            </ul>
            There's <strong>no need to add column headers</strong> in the CSV file.<br>
            Any <strong>cells without data</strong> in will be <strong>skipped during the import</strong>.
        </span>

    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Import Contact(s)</button>
    </div>
</form>