<h2>Create new tag</h2>

<form class="add-tag" name="tag" method="POST" action="/contacts/tags/create">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="tag-name">Tag Name*</label>
        <input type="text" class="form-control" id="tag-name" name="tag-name" required>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Create Tag</button>
    </div>
</form>

<h2>Assign a tag to a contact</h2>

<form class="assign-tag" name="assign-tag" method="POST" action="/contacts/tags/assign">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="contact-id">Select a Contact</label>
        <select class="form-control" id="contact-id" name="contact-id" required>
            @foreach ($contacts as $contact)
                <option value="{{ $contact->id }}">{{ $contact->first_name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="tag-id">Select a tag</label>
        <select class="form-control" id="tag-id" name="tag-id" required>
            @foreach ($tags as $tag)
                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Assign Tag</button>
    </div>
</form>