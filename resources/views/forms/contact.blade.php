<form class="add-contact" name="contact" method="POST" action="/" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="first-name">First Name*</label>
        <input type="text" class="form-control" id="first-name" name="first-name"
               pattern="[A-Za-z]+" title="Only letters" required>
    </div>

    <div class="form-group">
        <label for="surname">Surname*</label>
        <input type="text" class="form-control" id="surname" name="surname"
               pattern="[A-Za-z]+" title="Only letters" required>
    </div>

    <div class="form-group">
        <label for="home-address">Home Address</label>
        <textarea class="form-control" rows="4" id="home-address" name="home-address"></textarea>
    </div>

    <div class="form-group">
        <label for="email-address">Email Address</label>
        <input type="text" class="form-control" id="email-address" name="email_address">
    </div>

    <div class="form-group">
        <label for="phone_number">Phone Number</label>
        <input type="text" class="form-control" id="phone_number" name="phone_number"
               pattern="[0-9]+" title="Only numbers">
    </div>

    <div class="form-group">
        <label for="website">Website</label>
        <input type="text" class="form-control" id="website" name="website">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Add Contact</button>
    </div>
</form>