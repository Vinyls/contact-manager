<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="addContact">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button btn btn-default" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2>Add Contact</h2>
            </div>
            <div class="modal-body">
                <div>
                    @include ('forms.contact')
                </div>
            </div>
        </div>
    </div>
</div>