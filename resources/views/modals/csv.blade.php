<div class="modal fade" id="importContact" tabindex="-1" role="dialog" aria-labelledby="importContact">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button btn btn-default" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2>Import Contact(s)</h2>
            </div>
            <div class="modal-body">
                <div>
                    @include ('forms.csv')
                </div>
            </div>
        </div>
    </div>
</div>