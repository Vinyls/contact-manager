@include ('partials.tags')

<div class="table-responsive">
    <table class="contacts dataTable display table-responsive">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Home Address</th>
            <th>Email Address</th>
            <th>Phone Number</th>
            <th>Website</th>
            <th>Tags</th>
            <th>Added</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($contacts as $contact)
            <tr class="clickable-row" data-href='/contacts/{{ $contact->id }}'>

                <td class="first-name">{{ $contact->first_name }}</td>
                <td class="surname">{{ $contact->surname }}</td>
                <td>{{ $contact->home_address }}</td>
                <td>{{ $contact->email_address }}</td>
                <td>{{ $contact->phone_number }}</td>
                <td>{{ $contact->website }}</td>
                <td>
                    @foreach($contact->tags as $tag)
                        {{ $tag->name }}
                    @endforeach
                </td>
                <td>{{ $contact->created_at->toFormattedDateString() }}</td>

            </tr>
        @endforeach
        </tbody>
    </table>
</div>