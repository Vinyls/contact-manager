@extends('layout')

@section('content')
    <div class="container-fluid site-wrap">
        <header>
            <h1>Contact Manager</h1>
        </header>

        <main class="row container table-wrapper">
            <h2>Contact List</h2>

            <div class="action-buttons">
                <button type="button" class="btn btn-primary create-tag" data-toggle="modal" data-target="#createTag">
                    Manage Tags
                </button>

                <button type="button" class="btn btn-primary import-contact" data-toggle="modal" data-target="#importContact">
                    Import Contact(s) via CSV
                </button>

                <button type="button" class="btn btn-primary add-contact" data-toggle="modal" data-target="#addContact">
                    Add Contact
                </button>
            </div>

            <hr>

            @include ('tables.contact')

            @include ('partials.errors')
            @include ('partials.success')

            @include ('modals.contact')
            @include ('modals.tag')
            @include ('modals.csv')
        </main> <!-- .table-wrapper -->
    </div> <!-- .site-wrap -->
@endsection