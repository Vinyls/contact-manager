# Contact Manager

## Prerequisites

- [Composer](https://getcomposer.org/)

## Installation

- Clone the project
- Make a copy of the `.env.example` file and name it `.env`
- Create an empty mysql database on localhost called `contact_manager`
- Change the values for `DB_USERNAME` and `DB_PASSWORD` in the `.env` file
- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan serve` and visit the IP address this command provides you with

If you run into any issues, drop me an email at sammyabukmeil@gmail.com

## Frameworks, libraries and tools used

- [Laravel](https://laravel.com/)
- [DataTables](https://datatables.net/)
- [CSV](http://csv.thephpleague.com/)
- [jQuery](https://jquery.com/)
- [Bootstrap](http://getbootstrap.com/)
- [Sass](http://sass-lang.com/)
