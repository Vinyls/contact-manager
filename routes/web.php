<?php

Route::get('/', 'ContactsController@index');
Route::post('/', 'ContactsController@store');

//Route::get('/contacts/{contact}', 'ContactsController@show');

Route::get('/contacts/tags/{tag}', 'TagsController@index');
Route::post('/contacts/tags/create', 'TagsController@store');
Route::post('/contacts/tags/assign', 'TagsController@assign');

Route::post('/csv', 'CsvController@store');